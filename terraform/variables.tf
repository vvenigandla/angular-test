variable "docker_host" {
    type        = string
    description = "Use the default, do not modify."
    default     = "unix:///var/run/docker.sock"
}

variable "datadog_network" {
    type        = string
    description = "The docker datadog network ID..."
    default     = "3btlcmptvt4gxi09ifo9jlcs7"
}

variable "service_name" {
    type        = string
    description = "The name of the services application."
}

variable "app_port" {
    type        = string
    description = "The customer facing port on the host."
}

variable "app_yaml" {
    type        = string
    description = "The application configuration name within docker config."
}

variable "app_yaml_path" {
    type        = string
    description = "The path to the applications configuration file, within the container."
}

variable "app_yaml_id" {
    type        = string
    description = "The docker swarm ID of the application configuration docker config."
}

variable "cacerts_id" {
    type        = string
    description = "The docker swarm ID of the cacerts docker config."
}

variable "cacerts_path" {
    type        = string
    description = "The path to the cacerts file, within the container."
}

variable "cacerts" {
    type        = string
    description = "The cacerts name, within docker config."
}

variable "arccorp_jks" {
    type        = string
    description = "The arccorp_jks name, within docker config."
}

variable "arccorp_jks_id" {
    type        = string
    description = "The docker swarm ID of the arccorp_jks_id docker config."
}

variable "arccorp_jks_path" {
    type        = string
    description = "The path to the arccorp_jks file, within the container."
}

variable "environment" {
    type        = string
    description = "The environment to deploy the application."
    default     = "intg"
}

variable "tagname" {
    type        = string
    description = "The image tag name."
}

variable "registry" {
    type        = string
    description = "DO NOT UPDATE! The gitlab.com registry URL. Set in the .gitlab-ci.yml file."
}

variable "registryusername" {
    type        = string
    description = "DO NOT UPDATE! The gitlab.com registry username. Set in the .gitlab-ci.yml file."
}

variable "registrypassword" {
    type        = string
    description = "DO NOT UPDATE! The gitlab.com registry password. Set in the .gitlab-ci.yml file."
}

variable "namespace" {
    type        = string
    description = "DO NOT UPDATE! The gitlab.com project namespace. Set in the .gitlab-ci.yml file."
}

variable "projectname" {
    type        = string
    description = "DO NOT UPDATE! The gitlab.com project name. Set in the .gitlab-ci.yml file."
}

variable "force_update" {
    type        = number
    description = "Cause the service to force an update."
}