terraform {
  backend "http" {}

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.15.0"
    }
  }
  required_version = ">= 0.13"
}

provider "docker" {
  host = var.docker_host

  registry_auth {
    address  = var.registry
    username = var.registryusername
    password = var.registrypassword
  }
}
